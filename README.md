4th Year Course - Security
-------------------------------------------

Project Members
---------------
Simão Reis 

Rafael Saraiva

Project Description
-------------------
The goal of this project was to develop a flexible protection mechanism for protecting the confidentiality and integrity of data exchanged with files.

File protection through encryption can be performed by dozens of tools, but they all lack the flexibility to allow users to use their own encryption algorithms and strategies. Those tools only allow users to use the facilities they implement, and do not allow users to implement personal variants of existing algorithms or even completely new encryption algorithms.

For  implementing  a  flexible  file  protection  strategy  we implement  a  generic  file  protection  engine,  which  is  in  practice  the main application used for encrypting and decrypting files and ensuring their integrity.   Both  encryption/decryption  and  integrity  validation  should  be password-based, and no key distribution strategy was required.

The  engine  is  used  with  one  or  more  selected  protection  plugins (PPs). For file encryption, the user selects the intended encryption/decryption PP, which is then used to encrypt the file's contents, and that PP is then packed along the resulting Encrypted Data (ED), or cryptogram.  In otherwords,  a  protected  file is  composed  by  (i)  the  cryptogram  resulting from the encryption of the original file contents and (ii) the PP that should be used to recover the original file contents by decrypting the cryptogram. The PP consists of code, binary objects, or any other constructs which effectively can be used to decrypt the cryptogram.  We call such bundlea File Protection Container (FPC).

Besides  content  encryption,  the  students  should  implement  as  well  acontent integrity validation using a plugin-based MAC (Message Authenti-cation Code) strategy.  The same password used for encryption/decryptioncan  be  used  to  compute  MACs.   Naturally,  thePPused  to  compute  theMAC should be part of the finalED, and thatPPshould implement bothMAC computations and validations.

Plausible deniability was implemented as an extra feature. Plausible deniability can have many interpretations, but ultimately it allows the file owner to convince others that there are no encrypted contents on it (e.g. using steganography) or to reveal  innocuous  encrypted  contents  once  forced  to  do  so  (by  using  different keys or passwords to reveal different encrypted contents, some of them innocuous).

Implementation
--------------
For defining the format of a FPC, ZIP  file was used.

For dynamic PP loading and binding, native Java class loading mechanisms was used for dealing with PPs implemented as Java classes.